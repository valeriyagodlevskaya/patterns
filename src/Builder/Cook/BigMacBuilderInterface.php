<?php

namespace Patterns\Builder\Cook;

interface BigMacBuilderInterface
{
    public function reset(): void;
    public function addBun(string $ingredient): void;
    public function addPigMeat(string $ingredient): void;
    public function addSalad(string $ingredient): void;
    public function addChickenMeat(string $ingredient): void;
    public function addCheese(string $ingredient): void;
    public function addSouse(string $ingredient): void;
    public function addPickle(string $ingredient): void;

}