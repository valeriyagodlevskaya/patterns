<?php

namespace Patterns\Builder\Cook;


class Cook
{
    private BigMacBuilderInterface $bigMacBuilder;

    public function setBuilder(BigMacBuilderInterface $builder)
    {
        $this->bigMacBuilder = $builder;
        //возвращаем текущий обьект
        return $this;
    }

    public function makeStandardBigMac()
    {
        $builder = $this->getBuilder();
        $builder->addBun('2 булочки');
        $builder->addPigMeat('2 свинные котлеты');
        $builder->addCheese('Сыр');
        $builder->addPickle('соленый огурец');
        $builder->addSouse('Соус');

        return $builder->getBigMac();
    }

    public function makeChickenBigMac()
    {
        $builder = $this->getBuilder();
        $builder->addBun('2 булочки');
        $builder->addChickenMeat('1 куриная котлета');
        $builder->addSalad('Листья салата');
        $builder->addSouse('Соус');

        return $builder->getBigMac();
    }

    public function makeBigMac()
    {
        $builder = $this->getBuilder();
        return $builder;
    }

    public function getBuilder()
    {
        return $this->bigMacBuilder;
    }

}