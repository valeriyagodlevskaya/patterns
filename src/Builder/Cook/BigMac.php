<?php

namespace Patterns\Builder\Cook;

class BigMac
{
    private array $ingredients;

    public function setIngredient(string $ingredient)
    {
        $this->ingredients[] = $ingredient;
    }

    public function getIngredients()
    {
        return $this->ingredients;
    }
}