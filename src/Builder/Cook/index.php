<?php
namespace Patterns\Builder\Cook;

require __DIR__ . '/../../../vendor/autoload.php';

/**
 * Builder - паттерн позводяет создавать сложные обьекты пошагово.
 * Строитель дает возможность использовать один и тот же код строительства
 * для получения разных представлений обьектов.
 * Позволяет собираться обьекты пошагово, вызывая те шаги которые нужны.
*/


$builder = new BigMacBuilder();
$cook = new Cook();
$cook->setBuilder($builder);

echo "####Готовим Биг Мак стандарт.####" . PHP_EOL;
$bigMac = $cook->makeStandardBigMac();
echo implode(PHP_EOL, $bigMac->getIngredients()) . PHP_EOL;

//очищаем от предидущих ингркдиентов
$builder->reset();

echo "####Готовим Куринный Биг Мак.###" . PHP_EOL;
$bigMacChicken = $cook->makeChickenBigMac();
echo implode(PHP_EOL, $bigMacChicken->getIngredients()) . PHP_EOL;

//очищаем от предидущих ингркдиентов
$builder->reset();
echo "####Собираем свой Биг Мак.###" . PHP_EOL;
$cookYourBurger = $cook->makeBigMac();
$cookYourBurger->addBun('2 булочки');
$cookYourBurger->addCheese('Сыр');
$cookYourBurger->addSalad('Салат');
$yourBurger = $builder->getBigMac();
echo implode(PHP_EOL, $yourBurger->getIngredients()) . PHP_EOL;

