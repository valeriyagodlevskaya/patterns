<?php

namespace Patterns\Builder\Cook;


class BigMacBuilder implements BigMacBuilderInterface
{
    private BigMac $bigMac;

    public function __construct()
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->bigMac = new BigMac();
    }

    public function addBun($ingredient): void
    {
        $this->bigMac->setIngredient($ingredient);
    }

    public function addPigMeat($ingredient): void
    {
        $this->bigMac->setIngredient($ingredient);
    }

    public function addSalad($ingredient): void
    {
        $this->bigMac->setIngredient($ingredient);
    }

    public function addChickenMeat($ingredient): void
    {
        $this->bigMac->setIngredient($ingredient);
    }

    public function addCheese($ingredient): void
    {
        $this->bigMac->setIngredient($ingredient);
    }

    public function addSouse($ingredient): void
    {
        $this->bigMac->setIngredient($ingredient);
    }

    public function addPickle($ingredient): void
    {
        $this->bigMac->setIngredient($ingredient);
    }

    public function getBigMac(): BigMac
    {
        return $this->bigMac;
    }
}