<?php

namespace Patterns\Builder\ApartmentDesign;

class ChiefBuilder
{
    private ApartmentDesignBuilderInterface $builder;

    public function __construct(ApartmentDesignBuilderInterface $builder)
    {
        $this->builder = $builder;
        return $this;
    }

    public function getBuilder()
    {
        return $this->builder;
    }

    public function makeRoom()
    {
        $builder = $this->getBuilder();
        $builder->setConcrete();
        $builder->setRedPaint();
        $builder->setDoor();
        $builder->setSofa();

        return $builder->getApartmentDesign();
    }

    public function makeBathroom()
    {
        $builder = $this->getBuilder();
        $builder->setConcrete();
        $builder->setGrayTile();
        $builder->setBath();
        $builder->setMirror();
        $builder->setDoor();

        return $builder->getApartmentDesign();
    }



}