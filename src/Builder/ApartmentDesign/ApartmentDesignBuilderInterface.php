<?php

namespace Patterns\Builder\ApartmentDesign;

interface ApartmentDesignBuilderInterface
{
    public function reset(): void;
    public function setRedPaint() :void;
    public function setGrayTile() :void;
    public function setConcrete() :void;
    public function setDoor() :void;
    public function setBath() :void;
    public function setMirror() :void;
    public function setSofa() :void;

}