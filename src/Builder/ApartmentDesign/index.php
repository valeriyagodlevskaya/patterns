<?php
namespace Patterns\Builder\ApartmentDesign;

require __DIR__ . '/../../../vendor/autoload.php';


$builder = new ApartmentDesignBuilder();
$chiefBuilder = new ChiefBuilder($builder);

echo "Создаем дизайн комнаты:" . PHP_EOL;
$room = $chiefBuilder->makeRoom();
echo "###Материалы:####" . PHP_EOL;
echo implode(PHP_EOL, $room->getMaterials()) . PHP_EOL;
echo "###Мебель:####" . PHP_EOL;
echo implode(PHP_EOL, $room->getFurniture()) . PHP_EOL;

$builder->reset();//обнуляем

echo "Создаем дизайн ванной:" . PHP_EOL;
$bathroom = $chiefBuilder->makeBathroom();
echo "####Материалы:#####" . PHP_EOL;
echo implode(PHP_EOL, $bathroom->getMaterials()) . PHP_EOL;
echo "####Мебель:####" . PHP_EOL;
echo implode(PHP_EOL, $bathroom->getFurniture()) . PHP_EOL;