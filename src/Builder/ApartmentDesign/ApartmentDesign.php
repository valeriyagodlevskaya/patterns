<?php

namespace Patterns\Builder\ApartmentDesign;

class ApartmentDesign
{
    private array $materials;
    private array $furniture;

    public function setMaterials(string $material)
    {
        $this->materials[] = $material;
    }

    /**
     * @return array
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    public function setFurniture(string $thing)
    {
        $this->furniture[] = $thing;
    }

    public function getFurniture()
    {
        return $this->furniture;
    }

}