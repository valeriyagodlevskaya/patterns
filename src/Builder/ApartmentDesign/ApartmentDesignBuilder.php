<?php

namespace Patterns\Builder\ApartmentDesign;

class ApartmentDesignBuilder implements ApartmentDesignBuilderInterface
{
    private ApartmentDesign $apartmentDesign;

    public function __construct()
    {
        $this->reset();
    }

    public function reset(): void
    {
        $this->apartmentDesign = new ApartmentDesign();
    }

    public function setRedPaint(): void
    {
        $this->apartmentDesign->setMaterials('Красная краска');
    }

    public function setGrayTile(): void
    {
        $this->apartmentDesign->setMaterials('Серая плитка');
    }

    public function setConcrete(): void
    {
        $this->apartmentDesign->setMaterials('Бетон');
    }

    public function setDoor(): void
    {
        $this->apartmentDesign->setFurniture('Дверь');
    }

    public function setBath(): void
    {
        $this->apartmentDesign->setFurniture('Ванная');
    }

    public function setMirror(): void
    {
        $this->apartmentDesign->setFurniture('Зеркало');
    }

    public function setSofa(): void
    {
        $this->apartmentDesign->setFurniture('Диван');
    }

    public function getApartmentDesign()
    {
        return $this->apartmentDesign;
    }
}