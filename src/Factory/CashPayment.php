<?php

namespace Patterns\Factory;

class CashPayment
{
    public function pay(Order $order)
    {
        echo 'Cash pay success ' . $order->getSum() . '$.' . PHP_EOL;
    }

}