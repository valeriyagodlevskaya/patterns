<?php
namespace Patterns\Factory;

class BankPayment
{
    public function pay(Order $order)
    {
        echo 'Bank pay success '.$order->getSum() . '$.' . PHP_EOL;
    }

}