<?php

namespace Patterns\Factory;

require __DIR__ . '/../../vendor/autoload.php';

//Factory - опредлиляет интерфейс в суперклассе, позволяя подклассам изменять тип создаваемых обьектов

//формирование заказа
$orderData = [
    [
        'order' => new Order(500),
        'paymentType' => 'cash'
    ],
    [
        'order' => new Order(2000),
        'paymentType' => 'bank'
    ]
];

$cashPaymentService = new CashPayment();
$bankPaymentService = new BankPayment();

foreach ($orderData as $orderDataItem) {
    if ($orderDataItem['paymentType'] === 'cash') {
        $cashPaymentService->pay($orderDataItem['order']);
    }
    if ($orderDataItem['paymentType'] === 'bank') {
        $bankPaymentService->pay($orderDataItem['order']);
    }
}