<?php

namespace Patterns\Factory;

class Order
{
    private $sum;

    public function __construct($sum)
    {
        $this->sum = $sum;
    }

    public function getSum()
    {
        return $this->sum;
    }

}