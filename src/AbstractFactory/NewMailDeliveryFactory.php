<?php

namespace Patterns\AbstractFactory;

class NewMailDeliveryFactory implements AbstractFactoryInterface
{

    public function createDeliveryService(): DeliveryServiceInterface
    {
        return new NewMailDeliveryService();
    }

    public function createPackage(): PackageInterface
    {
        return new NewMailPackage();
    }
}