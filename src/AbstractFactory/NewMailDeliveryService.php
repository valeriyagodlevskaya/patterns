<?php

namespace Patterns\AbstractFactory;


class NewMailDeliveryService implements DeliveryServiceInterface
{
    public function sendPackage(PackageInterface $package): void
    {
        echo 'Отправляем поссылку Новой почтой.' . PHP_EOL;
    }
}