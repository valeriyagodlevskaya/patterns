<?php

namespace Patterns\AbstractFactory;

interface PackageInterface
{
    public function getConsist(): void;

}