<?php

namespace Patterns\AbstractFactory;

class UkrPackage implements PackageInterface
{
    public function getConsist(): void
    {
        echo 'Проверка поссылки УкрПочти' . PHP_EOL;
    }
}