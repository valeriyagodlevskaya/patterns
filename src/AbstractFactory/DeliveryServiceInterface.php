<?php

namespace Patterns\AbstractFactory;

interface DeliveryServiceInterface
{
    public function sendPackage(PackageInterface $package): void;

}