<?php

namespace Patterns\AbstractFactory;

class UkrDeliveryFactory implements AbstractFactoryInterface
{
    public function createDeliveryService(): DeliveryServiceInterface
    {
        return new UkrDeliveryService();
    }

    public function createPackage(): PackageInterface
    {
        return new UkrPackage();
    }
}