<?php
namespace Patterns\AbstractFactory;

require __DIR__ . '/../../vendor/autoload.php';

//Abstract Factory - паттерн позволяет создавать семейства связанных обьектов,
// не привязываясь к конкретным классам создаваемых обьектов.

function delivery(array $factories) {
    foreach ($factories as $factory) {
        //получаем сервис доставки
        $deliveryService = $factory->createDeliveryService();
        //получаем поссылку
        $package = $factory->createPackage();
        //проверяем поссылку
        $package->getConsist();
        //отправляем поссылку
        $deliveryService->sendPackage($package);
    }

}

$factories = [
    new UkrDeliveryFactory(),
    new NewMailDeliveryFactory()
];

delivery($factories);