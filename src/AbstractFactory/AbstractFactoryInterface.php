<?php

namespace Patterns\AbstractFactory;

interface AbstractFactoryInterface
{
    public function createDeliveryService(): DeliveryServiceInterface;

    public function createPackage(): PackageInterface;
}