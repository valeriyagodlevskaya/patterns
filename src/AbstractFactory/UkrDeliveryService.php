<?php

namespace Patterns\AbstractFactory;


class UkrDeliveryService implements DeliveryServiceInterface
{
    public function sendPackage(PackageInterface $package): void
    {
        echo 'Отправляем поссылку через УкрПочту.' . PHP_EOL;
    }
}