<?php

namespace Patterns\AbstractFactory;


class NewMailPackage implements PackageInterface
{
    public function getConsist(): void
    {
        echo 'Проверка поссылки Новой почти' . PHP_EOL;
    }
}