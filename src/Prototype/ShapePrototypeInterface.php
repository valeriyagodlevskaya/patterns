<?php

namespace Patterns\Prototype;

interface ShapePrototypeInterface
{
    public function clone();

}