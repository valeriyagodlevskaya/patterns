<?php

namespace Patterns\Prototype;

class ShapePrototype
{
    protected string $name;

    public function __construct(string $name) {
        $this->name = $name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
}