<?php

namespace Patterns\Prototype;

class SQLBookPrototype extends BookPrototype
{

    public function __construct()
    {
        $this->topic = 'SQL';
    }

    function clone()
    {
        return clone $this;
    }
}