<?php

namespace Patterns\Prototype;

class PHPBookPrototype extends BookPrototype
{

    public function __construct()
    {
        $this->topic = 'PHP';
    }

    function clone()
    {
        return clone $this;
    }
}