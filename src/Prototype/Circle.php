<?php

namespace Patterns\Prototype;

class Circle implements ShapePrototypeInterface
{
    private string $title;

    public function __construct()
    {
        $this->title = 'Круг';
    }

    public function clone()
    {
        return clone $this;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
}