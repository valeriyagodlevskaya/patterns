<?php

namespace Patterns\Prototype;

abstract class BookPrototype
{
    protected string $title;
    protected string $topic;

    abstract function clone();

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getTopic()
    {
        return $this->topic;
    }
}