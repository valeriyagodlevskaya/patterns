<?php

namespace Patterns\Prototype;

require __DIR__ . '/../../vendor/autoload.php';

/**
 * Прототип - паттерн который позволяет копировать обьекты, не вдаваясь в подробности их реализации.
 * Prototype создает объект, который основан на уже существующем объекте посредством клонирования
 */

echo "###Method 1####" . PHP_EOL;
$circle = new Circle();
echo $circle->getTitle() . PHP_EOL;
$clone = $circle->clone();
echo $clone->getTitle() . PHP_EOL;
$cloneModify = $clone->clone();
$cloneModify->setTitle('ромб');
echo $cloneModify->getTitle() . PHP_EOL;

echo "###Method 2####" . PHP_EOL;
$shape = new ShapePrototype('квадрат');
echo $shape->getName() . PHP_EOL;
$cloneShape = clone $shape;
$cloneShape->setName('circle2');
echo $cloneShape->getName() . PHP_EOL;

echo "###Method 3####" . PHP_EOL;
$phpPrototype = new PHPBookPrototype();
$sqlPrototype = new SQLBookPrototype();

$book1 = $phpPrototype->clone();
$book1->setTitle('PHP Book 2');
echo "Book title: " . $book1->getTitle() . PHP_EOL;
echo "Book topic: " . $book1->getTopic() . PHP_EOL;