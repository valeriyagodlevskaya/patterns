<?php

namespace Patterns\Adapter;

class XMLToArrayReportAdapter implements ArrayReportInterfaceAdapter
{
    private XMLReport $xmlReport;

    public function __construct(XMLReport $xmlReport)
    {
        $this->xmlReport = $xmlReport;
    }

    public function getData(): array
    {
        $xmlData = $this->xmlReport->buildXML();
        $xml = simplexml_load_string($xmlData);
        $reportData = json_decode(json_encode($xml), true)['element'];

        return $reportData;
    }
}