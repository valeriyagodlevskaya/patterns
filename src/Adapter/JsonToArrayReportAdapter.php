<?php

namespace Patterns\Adapter;

class JsonToArrayReportAdapter implements ArrayReportInterfaceAdapter
{
    private JsonReport $jsonReport;

    public function __construct(JsonReport $jsonReport)
    {
        $this->jsonReport = $jsonReport;
    }

    public function getData(): array
    {
        $data = $this->jsonReport->buildJson();

        return json_decode($data, true);
    }
}