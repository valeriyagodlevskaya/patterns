<?php

namespace Patterns\Adapter;

require __DIR__ . '/../../vendor/autoload.php';

/**
 * Адаптер — это структурный паттерн проектирования, который позволяет объектам с несовместимыми интерфейсами работать вместе.
 * Адаптер — это класс, который может одновременно работать и с клиентом, и с сервисом.
 * Он реализует клиентский интерфейс и содержит ссылку на объект сервиса.
 * Адаптер получает вызовы от клиента через методы клиентского интерфейса,
 * а затем переводит их в вызовы методов обёрнутого объекта в правильном формате.
 *
 * Адаптер приводит несовместимые обьекты к общему интерфейсу и содержит только логику преобразования
 */

$listReport = [
    new ArrayReport(),
    new JsonReport(),
    new XMLReport()
];

function run(array $reports) {
    $reportData = null;
    foreach ($reports as $report) {
        if ($report instanceof ArrayReport) {
            $reportData = $report;
        } elseif ($report instanceof JsonReport) {
            $reportData = (new JsonToArrayReportAdapter($report));
        } elseif ($report instanceof XMLReport) {
            $reportData = (new XMLToArrayReportAdapter($report));
        }
        if (is_null($reportData)) {
            print_r('Empty report');
        }

        renderView($reportData);
    }
}

function renderView(ArrayReportInterfaceAdapter $adapter) {
    print_r($adapter->getData());
}

run($listReport);
