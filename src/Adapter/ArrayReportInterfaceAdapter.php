<?php

namespace Patterns\Adapter;

interface ArrayReportInterfaceAdapter
{
    public function getData(): array;

}