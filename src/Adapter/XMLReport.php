<?php

namespace Patterns\Adapter;

class XMLReport
{
    public function buildXML(): string
    {
        return '<?xml version="1.0" encoding="UTF-8"?>
                <root>
                    <element>
                        <name>iPhone</name>
                        <count>100</count>
                        <price>1000</price>
                    </element>
                    <element>
                        <name>Xiomi</name>
                        <count>300</count>
                        <price>500</price>
                    </element>
                    <element>
                        <name>Samsung</name>
                        <count>130</count>
                        <price>700</price>
                    </element>
                </root>
        ';
    }

}