<?php

namespace Patterns\Adapter;

class ArrayReport implements ArrayReportInterfaceAdapter
{
    public function getData(): array
    {
        return [
            [
                'name'  => 'iPhone',
                'price' => 1000,
                'count' => 100
            ],
            [
                'name'  => 'Xiomi',
                'price' => 500,
                'count' => 300
            ],
            [
                'name'  => 'Samsung',
                'price' => 700,
                'count' => 130
            ],
        ];
    }

}