<?php

namespace Patterns\Adapter;

class JsonReport
{
    public function buildJson(): string
    {
        return '[{"name":"iPhone", "price":900, "count":10},{"name":"Xiomi", "price":500, "count":800}]';
    }

}